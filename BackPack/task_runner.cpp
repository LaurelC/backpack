#include "task_runner.h"
#include <iostream>
#include <ostream>

#include "excel_builder.h"
#include "objects_creator.h"
#include "packer_combo.h"
#include "packer_dynamic.h"

using namespace std;

int task_runner::run_dyn_get_price(int* time, long long* steps, vector<content_object_int>* objects)
{
	backpack backpack(1000);
	packer_dynamic packer_dyn(&backpack, objects);
	packer_dyn.pack();

	*time += packer_dyn.get_work_time();
	*steps += packer_dyn.get_steps_count();
	return packer_dyn.get_result_price();
}


int task_runner::run_com_get_price(int* time, long long* steps, std::vector<content_object_int>* objects, const int greedy_steps)
{
	backpack backpack(1000);
	packer_combo packer_combine(&backpack, objects, greedy_steps);
	packer_combine.pack();

	*time += packer_combine.get_work_time();
	*steps += packer_combine.get_steps_count();
	return packer_combine.get_result_price();
}


void task_runner::run_similar_dyn(const int complexity_count, const int test_count) const
{
	cout << endl << "DYNAMIC SIMILAR" << endl;
	excel_builder_.empty_line();

	auto prev_time = 0;
	auto time = 0;
	long long prev_steps = 0;
	long long steps = 0;
	auto price = 0;
	auto obj_count = 10;

	vector<content_object_int> objects;
	for (auto complexity = 0; complexity < complexity_count; complexity++)
	{
		const objects_creator::task_data task_data_close(obj_count, 80, 100, 1000);
		for (auto test_id = 0; test_id < test_count; test_id++)
		{
			objects_creator::get_objects(&objects, task_data_close, test_id);

			price += run_dyn_get_price(&time, &steps, &objects);
		}
		cout << "Complexity " << complexity + 1 << " | time " << time - prev_time << endl;
		excel_builder_.add_line("dyn_sim", obj_count, steps - prev_steps, time - prev_time, price);
		prev_time = time;
		prev_steps = steps;
		obj_count *= 10;
	}

	cout << "dynamic whole time " << time << " whole price " << price << endl;
}


void task_runner::run_various_dyn(const int complexity_count, const int test_count) const
{
	cout << endl << "DYNAMIC VARIOUS" << endl;
	excel_builder_.empty_line();

	auto prev_time = 0;
	auto time = 0;
	long long prev_steps = 0;
	long long steps = 0;
	auto price = 0;
	auto obj_count = 10;

	vector<content_object_int> objects;
	for (auto complexity = 0; complexity < complexity_count; complexity++)
	{
		const objects_creator::task_data task_data_close(obj_count, 1, 100, 1000);
		for (auto test_id = 0; test_id < test_count; test_id++)
		{
			objects_creator::get_objects(&objects, task_data_close, test_id);

			price += run_dyn_get_price(&time, &steps, &objects);
		}
		cout << "Complexity " << complexity + 1 << " | time " << time - prev_time << endl;
		excel_builder_.add_line("dyn_var", obj_count, steps - prev_steps, time - prev_time, price);
		prev_time = time;
		prev_steps = steps;
		obj_count *= 10;
	}

	cout << "dynamic whole time " << time << " whole price " << price << endl;
}


void task_runner::run_similar_com(const int complexity_count, const int test_count, const int greedy_steps) const
{
	cout << endl << "COMBO SIMILAR" << endl;
	excel_builder_.empty_line();

	auto prev_time = 0;
	auto time = 0;
	long long prev_steps = 0;
	long long steps = 0;
	auto price = 0;
	auto obj_count = 10;

	vector<content_object_int> objects;
	for (auto complexity = 0; complexity < complexity_count; complexity++)
	{
		const objects_creator::task_data task_data_close(obj_count, 80, 100, 1000);
		for (auto test_id = 0; test_id < test_count; test_id++)
		{
			objects_creator::get_objects(&objects, task_data_close, test_id);

			price += run_com_get_price(&time, &steps, &objects, greedy_steps);
		}
		cout << "Complexity " << complexity + 1 << " | time " << time - prev_time << endl;
		excel_builder_.add_line("com_sim_" + std::to_string(greedy_steps), obj_count, steps - prev_steps, time - prev_time, price);
		prev_time = time;
		prev_steps = steps;
		obj_count *= 10;
	}

	cout << "combine whole time " << time << " whole price " << price << endl;
}


void task_runner::run_various_com(const int complexity_count, const int test_count, const int greedy_steps) const
{
	cout << endl << "COMBO VARIOUS" << endl;
	excel_builder_.empty_line();
	auto prev_time = 0;
	auto time = 0;
	long long prev_steps = 0;
	long long steps = 0;
	auto price = 0;
	auto obj_count = 10;

	vector<content_object_int> objects;
	for (auto complexity = 0; complexity < complexity_count; complexity++)
	{
		const objects_creator::task_data task_data_close(obj_count, 1, 100, 1000);
		for (auto test_id = 0; test_id < test_count; test_id++)
		{
			objects_creator::get_objects(&objects, task_data_close, test_id);

			price += run_com_get_price(&time, &steps, &objects, greedy_steps);
		}
		cout << "Complexity " << complexity + 1 << " | time " << time - prev_time << endl;
		excel_builder_.add_line("com_var_" + std::to_string(greedy_steps), obj_count, steps - prev_steps, time - prev_time, price);
		prev_time = time;
		prev_steps = steps;
		obj_count *= 10;
	}

	cout << "combine whole time " << time << " whole price " << price << endl;
}


void task_runner::run_all(const int complexity_count, const int test_count) const
{
	run_various_dyn(complexity_count, test_count);
	run_similar_dyn(complexity_count, test_count);
}
