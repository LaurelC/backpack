#include "objects_creator.h"

#include <cstdlib>
#include <ctime>
#include <iostream>

/*
bool objects_creator::get_objects(std::vector<content_object_int>* objects, const int objects_count, const int min_weight, const int max_weight, const int max_price)
{
	if (objects_count <= 0 || max_weight <= 0 || max_price <= 0 || objects == nullptr) return false;

	srand(time(nullptr));
	for (auto i = 0; i < objects_count; i++)
	{
		auto* object = new content_object_int(i, rand() % (max_price - 1) + 1, min_weight + rand() % (max_weight - min_weight - 1) + 1);
		(*objects).push_back(*object);
	}

	return true;
}
*/

bool objects_creator::get_objects(std::vector<content_object_int>* objects, const task_data task, const int seed)
{
	if (task.objects_count <= 0 || task.max_weight <= 0 || task.max_price <= 0 || objects == nullptr) return false;

	srand(seed);
	if (!objects->empty()) {
		objects->clear();
		//objects = new std::vector<content_object_int>();
	}
		
	for (auto i = 0; i < task.objects_count; i++)
	{
		const auto rand_price = rand() % (task.max_price - 1) + 1;
		const auto rand_weight = task.min_weight + rand() % (task.max_weight - task.min_weight - 1) + 1;
		//auto* object = new content_object_int(i, rand() % (task.max_price - 1) + 1, task.min_weight + rand() % (task.max_weight - task.min_weight - 1) + 1);
		auto* object = new content_object_int(i, rand_price, rand_weight);
		(*objects).push_back(*object);
	}

	return true;
}
