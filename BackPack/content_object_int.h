#pragma once
#include "content_object.h"

struct content_object_int : content_object
{
	int price;
	int weight;

	content_object_int(const int id_in, const int price_in, const int weight_in) : content_object(id_in), price(price_in), weight(weight_in){}
};
