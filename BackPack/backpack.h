#pragma once
#include <vector>

struct backpack
{
	int capacity;
	std::vector<int> objects_id;

	explicit backpack(const int capacity_in): capacity(capacity_in){}
};