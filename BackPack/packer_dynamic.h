#pragma once
#include "packer_abstract.h"
#include <vector>

class packer_dynamic : public packer_abstract
{
protected:
	std::vector<int> table_;

public:
	packer_dynamic(backpack* backpack, std::vector<content_object_int>* objects)
		: packer_abstract(backpack, objects)
	{
		table_ = std::vector<int>(objects_->size() * static_cast<int>(backpack_->capacity + 1), 0);
	}

protected:
	int get_cell_id(int cur_capacity, int cur_object_id) const;
	void set_first_prices();
	void set_capacity_object_price(int object_id, int capacity);
	void set_object_prices(int object_id);
	void print_progress_table() override;
public:
	bool pack() override;
	int get_work_time() override;
	int get_result_price() override;
	int get_steps_count() override;
};