#include "packer_combo.h"

#include <ctime>

using namespace std;

bool packer_combo::is_used(const int id)
{
	for (auto used_id : used_ids_) {
		if (used_id == id)
			return true;
	}
	return false;
}

int packer_combo::get_expensive_object_id()
{
	auto is_found = false;
	auto expensive_id = 0;
	for (auto id = 1; id < objects_->size(); id++)
	{
		if (objects_->at(id).price <= objects_->at(expensive_id).price)
			continue;

		if (objects_->at(id).weight > backpack_->capacity) continue;

		if (is_used(id)) continue;

		expensive_id = id;
		is_found = true;
	}
	if (is_found) return expensive_id;
	return -1;
}

void packer_combo::greedy_step(const int expensive_id)
{
	price_ += objects_->at(expensive_id).price;
	used_ids_.push_back(expensive_id);
	backpack_->capacity -= objects_->at(expensive_id).weight;
}


void packer_combo::set_capacity_used_object_price(const int object_id, const int capacity)
{
	const auto cell_id = get_cell_id(capacity, object_id);
	const auto prev_cell_id = get_cell_id(capacity, object_id-1);
	const auto prev_max = table_[prev_cell_id];
	table_[cell_id] = prev_max;
}


void packer_combo::set_used_object_prices(const int object_id)
{
	if(object_id == 0) return;
	for (auto cur_capacity = 0; cur_capacity <= backpack_->capacity; cur_capacity++)
	{
		set_capacity_used_object_price(object_id, cur_capacity);
	}
}

bool packer_combo::pack()
{
	time_started_ = clock();

	for (auto step = 0; step < greedy_steps_; step++)
	{
		const auto expensive_id = get_expensive_object_id();
		if (expensive_id != -1)
			greedy_step(expensive_id);
		else
			break;
		steps_++;
	}

	if (backpack_->capacity > 0) {
		table_ = std::vector<int>(objects_->size() * (backpack_->capacity + 1), price_);

		for (auto cur_object_id = 1; cur_object_id < objects_->size(); cur_object_id++)
		{
			if (is_used(cur_object_id))
				set_used_object_prices(cur_object_id);
			else 
			{
				set_object_prices(cur_object_id);
				steps_++;
			}
		}
	}

	//print_progress_table();

	time_ended_ = clock();

	return true;
}
