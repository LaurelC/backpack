#include "packer_dynamic.h"
#include <ctime>
#include <iostream>

using namespace std;

int packer_dynamic::get_cell_id(const int cur_capacity, const int cur_object_id) const
{
	return cur_object_id * (backpack_->capacity + 1) + (cur_capacity);
}


void packer_dynamic::set_first_prices()
{
	for (auto cur_capacity = 0; cur_capacity <= backpack_->capacity; cur_capacity++)
	{
		const auto cur_cell_id = get_cell_id(cur_capacity, 0);
		if (objects_->at(0).weight <= cur_capacity)
			table_[cur_cell_id] = objects_->at(0).price;
	}
}


void packer_dynamic::set_capacity_object_price(const int object_id, const int capacity)
{
	const auto cell_id = get_cell_id(capacity, object_id);
	const auto cell_weight = objects_->at(object_id).weight;
	const auto prev_cell_id = get_cell_id(capacity, object_id-1);

	const auto prev_max = table_[prev_cell_id];

	if (cell_weight <= capacity)
	{
		const auto left_object_weight = capacity - cell_weight;
		const auto left_cell_id = get_cell_id(left_object_weight, object_id - 1);
		const auto left_max = table_[left_cell_id];

		const auto possible_max = objects_->at(object_id).price + left_max;

		if (prev_max > possible_max)
			table_[cell_id] = prev_max;
		else
			table_[cell_id] = possible_max;
		steps_++;
	}
	else
		table_[cell_id] = prev_max;
}


void packer_dynamic::set_object_prices(const int object_id)
{
	for (auto cur_capacity = 0; cur_capacity <= backpack_->capacity; cur_capacity++)
	{
		set_capacity_object_price(object_id, cur_capacity);
	}
}


void packer_dynamic::print_progress_table()
{
	for (auto cur_object_id = 0; cur_object_id < static_cast<int>(objects_->size()); cur_object_id++)
	{
		for (auto cur_backpack_weight = 1; cur_backpack_weight <= (backpack_->capacity);
		     cur_backpack_weight++)
		{
			const auto cur_cell_id = cur_object_id * (backpack_->capacity + 1) + (
				cur_backpack_weight);
			cout << table_[cur_cell_id] << " ";
		}
		cout << endl;
	}
}


bool packer_dynamic::pack()
{
	time_started_ = clock();

	table_ = vector<int>(objects_->size() * (backpack_->capacity + 1), 0);

	set_first_prices();

	const auto objects_count = static_cast<int>(objects_->size());
	
	for (auto cur_object_id = 1; cur_object_id < objects_count; cur_object_id++)
		set_object_prices(cur_object_id);

	//print_progress_table();

	time_ended_ = clock();

	return true;
}


int packer_dynamic::get_work_time()
{
	if (time_ended_ == NULL) return -1;
	return time_ended_ - time_started_;
}


int packer_dynamic::get_result_price()
{
	return table_.back();
}

int packer_dynamic::get_steps_count()
{
	return steps_;
}
