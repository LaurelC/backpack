#pragma once
#include <vector>

#include "content_object_int.h"
#include "backpack.h"

class packer_abstract
{
protected:
	backpack* backpack_;
	std::vector<content_object_int>* objects_;
	int time_started_ = 0;
	int time_ended_ = 0;
	int steps_ = 0;
	//int result_price_ = 0;
	virtual void print_progress_table() = 0;
public:
	virtual ~packer_abstract() = default;
	virtual int get_work_time() = 0;
	virtual int get_result_price() = 0;
	virtual int get_steps_count() = 0;
	virtual bool pack() = 0;

	packer_abstract(backpack* backpack, std::vector<content_object_int>* objects)
	{
		backpack_ = backpack;
		objects_ = objects;
	}
};
