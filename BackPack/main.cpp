#include "excel_builder.h"
#include "task_runner.h"

using namespace std;
int main()
{
	auto* task_runner_ = new task_runner();
	
	task_runner_->run_similar_dyn(5, 50);
	task_runner_->run_similar_com(5, 50, 3);
	task_runner_->run_similar_com(5, 50, 5);
	task_runner_->run_similar_com(5, 50, 7);
	task_runner_->run_various_dyn(5, 50);
	task_runner_->run_various_com(5, 50, 3);
	task_runner_->run_various_com(5, 50, 5);
	task_runner_->run_various_com(5, 50, 7);
}