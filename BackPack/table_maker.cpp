#include "table_maker.h"

#include <iostream>

void table_maker::print_task(int backpack_capacity, objects_creator::task_data task_data, int work_time)
{
	std::cout << "capacity " << backpack_capacity << " | ";
	std::cout << "objects count " << task_data.objects_count << " | ";
	std::cout << "min weight " << task_data.min_weight << " | ";
	std::cout << "max weight " << task_data.max_weight << " | ";
	std::cout << "work time " << work_time << std::endl;
}
