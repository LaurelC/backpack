#pragma once
#include <vector>

#include "content_object_int.h"
#include "excel_builder.h"
class packer_abstract;

class task_runner
{
private:
	excel_builder excel_builder_;
	static int run_dyn_get_price(int* time, long long* steps, std::vector<content_object_int>* objects);
	static int run_com_get_price(int* time, long long* steps, std::vector<content_object_int>* objects, int greedy_steps);
public:
	void run_similar_dyn(int complexity_count ,int test_count) const;
	void run_similar_com(int complexity_count ,int test_count, int greedy_steps) const;
	void run_various_dyn(int complexity_count, int test_count) const;
	void run_various_com(int complexity_count, int test_count, int greedy_steps) const;
	void run_all(int complexity_count, int test_count) const;

	task_runner()
	{
		excel_builder_.build_file();
	}

	~task_runner()
	{
		excel_builder_.close_file();
	}
};

