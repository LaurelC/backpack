#include "excel_builder.h"
#include <iomanip>
#include <iostream>
#include <fstream>

void excel_builder::build_file()
{
	file_ = new fstream();
	file_->open(R"(E:/Dev/CPP/backpack/BackPack/tables.xls)", ios::out);
	(*file_) << "alg_name" << "\t" << "obj_count" << "\t" << "price" << "\t" << "steps" << "\t" << "time" << endl;
}

void excel_builder::empty_line() const
{
	(*file_) << endl;
}

void excel_builder::add_line(const string alg_name, const int obj_count, const long long steps, const int time, const int price) const
{
	(*file_) << alg_name << "\t" << obj_count << "\t" << price << "\t" << steps << "\t" << time << endl;
}

void excel_builder::close_file() const
{
	file_->close();
}
