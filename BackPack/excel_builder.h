#pragma once
#include <string>
#include <iomanip>

using namespace std;
class excel_builder
{
private:
	fstream* file_{};
public:
	void build_file();
	void empty_line() const;
	void add_line(string alg_name, int obj_count, const long long steps, int time, int price) const;
	void close_file() const;
};

