#pragma once
#include "packer_dynamic.h"
class packer_combo final :
	public packer_dynamic
{
private:
	int greedy_steps_;
	int price_;
	std::vector<int> used_ids_;

	bool is_used(int id);
	int get_expensive_object_id();
	void greedy_step(int expensive_id);
	void set_capacity_used_object_price(int object_id, int capacity);
	void set_used_object_prices(int object_id);
public:
	bool pack() override;

	packer_combo(backpack* backpack, std::vector<content_object_int>* objects, const int greedy_steps)
		: packer_dynamic(backpack, objects), greedy_steps_(greedy_steps)
	{
		price_ = 0;
		used_ids_ = std::vector<int>();
	}
};

