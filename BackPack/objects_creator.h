#pragma once
#include <vector>

#include "content_object_int.h"

class objects_creator
{
public:
	struct task_data
	{
		int objects_count;
		int min_weight;
		int max_weight;
		int max_price;

		task_data(const int objects_count_, const int min_weight_, const int max_weight_, const int max_price_) :
			objects_count(objects_count_),
			min_weight(min_weight_),
			max_weight(max_weight_),
			max_price(max_price_){}
	};
	
	//static bool get_objects(std::vector<content_object_int>* objects, int objects_count, int min_weight, int max_weight, int max_price);
	static bool get_objects(std::vector<content_object_int>* objects, task_data task, int seed);
};
